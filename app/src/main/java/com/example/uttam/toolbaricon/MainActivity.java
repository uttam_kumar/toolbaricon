package com.example.uttam.toolbaricon;

import android.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

public class MainActivity extends AppCompatActivity {
    private Toolbar mTopToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Synopi IPTalkie");

        android.support.v7.app.ActionBar ab=getSupportActionBar();

       // ab.setDisplayShowTitleEnabled(false);

        ab.setIcon(R.drawable.synopi_iptalkie_logo);
        //ab.setIcon(R.drawable.ic_action_name);

        //ab.setLogo(R.drawable.iptalkie_logo_without_backg);
        //ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_item, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
}
